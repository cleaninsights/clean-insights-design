{
  "$schema": "http://json-schema.org/draft/2019-09/schema#",
  "$id": "https://cleaninsights.org/schemas/configuration.schema.json",
  "title": "CleanInsights SDK Configuration",
  "description": "The scheme defining the JSON configuration file for CleanInsights SDK.",
  "type": "object",
  "properties": {
    "server": {
      "title": "CIMP Server Address",
      "description": "The fully qualified URI to the CIMP (CleanInsights Matomo Proxy) endpoint, where the gathered measurements will be offloaded.",
      "examples": ["https://matomo.example.org/ci/cleaninsights.php"],
      "type": "string",
      "format": "uri"
    },
    "siteId": {
      "title": "Matomo Site ID",
      "description": "The site ID used in the Matomo server which will collect and analyze the gathered data.",
      "examples": [1, 2, 3, 345345],
      "type": "integer",
      "minimum": 1
    },
    "timeout": {
      "title": "Network Timeout",
      "description": "Timeout in seconds used when trying to connect to the CIMP.",
      "examples": [2.5, 5, 10, 30],
      "default": 5,
      "type": "number",
      "minimum": 0.1
    },
    "maxRetryDelay": {
      "title": "Maximum Retry Delay",
      "description": "The SDK uses a truncated exponential backoff strategy on server failures. So the delay until it retries will rise exponentially, until it reaches `maxRetryDelay` seconds.",
      "default": 3600,
      "examples": [1800, 3600, 7200, 86400],
      "type": "number",
      "minimum": 0.1
    },
    "maxAgeOfOldData": {
      "title": "Maximum Age of Old Measurements",
      "description": "The number in days of how long the SDK will try to keep sending old measurements. If the measurements become older than that, they will be purged.",
      "default": 100,
      "examples": [14, 30, 100, 183, 365],
      "type": "integer",
      "minimum": 1
    },
    "persistEveryNTimes": {
      "title": "Frequency of Data Persistence",
      "description": "Number of times of measurements after when the collected data is persisted again. Set to one to have the SDK persist the recorded data after every measurement. Set it to a very high number, to never persist. You will need to make sure, that persistence is done by you, otherwise you will loose data!",
      "default": 10,
      "examples": [1, 10, 25, 99999999],
      "type": "integer",
      "minimum": 1
    },
    "serverSideAnonymousUsage": {
      "title": "Override Consents for server-side Usage",
      "description": "When set to true, assumes consent for all campaigns and none for features. Only use this, when you're running on the server and don't measure anything users might need to give consent to!",
      "default": false,
      "type": "boolean"
    },
    "debug": {
      "title": "CleanInsights SDK Debug Flag",
      "description": "When set to true, the CleanInsights SDK will log a debugging information to the console.",
      "default": false,
      "type": "boolean"
    },
    "campaigns": {
      "title": "Measurement Campaigns",
      "description": "Each insight you want to gain needs to be configured as a measurement campaign.",
      "type": "object",
      "patternProperties": {
        "^.+$": {
          "title": "Unique Campaign ID",
          "description": "Every campaign needs a unique identifier, which you use as an argument to a measurement call, so that the measurement can be checked against the campaign parameters.",
          "examples": ["my-special-campaign", "settingsUsage"],
          "type": "object",
          "properties": {
            "start": {
              "title": "Campaign Timeframe Start",
              "description": "Campaigns run for a certain time. Measurements trying to be done for this campaign before this point in time will be ignored.",
              "examples": ["2018-11-13T20:20:39+00:00", "2020-10-01T00:00:00-05:00"],
              "type": "string",
              "format": "date-time"
            },
            "end": {
              "title": "Campaign Timeframe End",
              "description": "Campaigns run for a certain time. Measurements trying to be done for this campaign after this point in time will be ignored.",
              "examples": ["2018-11-13T20:20:39+00:00", "2020-12-31T23:59:59-05:00"],
              "type": "string",
              "format": "date-time"
            },
            "aggregationPeriodLength": {
              "title": "Length of an Aggregation Period",
              "description": "The length of the aggregation period in number of days. At the end of a period, the aggregated data will be sent to the analytics server.",
              "examples": [1, 2, 3, 7, 14, 30],
              "type": "integer",
              "minimum": 1
            },
            "numberOfPeriods": {
              "title": "Number of Periods",
              "description": "The number of periods you want to measure in a row. Therefore the total length in days you measure one user is `aggregationPeriodLength * numberOfPeriods` beginning with the first day of the next period after the user consented.",
              "examples": [1, 2, 3],
              "default": 1,
              "type": "integer",
              "minimum": 1
            },
            "onlyRecordOnce": {
              "title": "Only-Record-Once Flag",
              "description": "Will result in recording only the first time a visit or event happened per period. Useful for yes/no questions.",
              "default": false,
              "type": "boolean"
            },
            "eventAggregationRule": {
              "title": "Event Value Aggregation Rule",
              "description": "The rule how to aggregate the value of an event (if any given) with subsequent calls. Either 'sum' or 'avg'.",
              "default": "sum",
              "type": "string",
              "enum": ["sum", "avg"]
            },
            "strengthenAnonymity": {
              "title": "Strengthen Anonymity",
              "description": "When set to true, measurements only ever start at the next full period. This ensures, that anonymity guaranties aren't accidentally reduced because the first period is very short.",
              "default": false,
              "type": "boolean"
            }
          },
          "additionalProperties": false,
          "if": {
            "properties": {
              "eventAggregationRule": {
              }
            }
          },
          "then": {
            "properties": {
              "onlyRecordOnce": {
                "const": false
              }
            }
          },
          "required": ["start", "end", "aggregationPeriodLength"]
        }
      }
    }
  },
  "additionalProperties": false,
  "required": ["server", "siteId", "campaigns"]
}