# Matomo Site ID Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/idsite
```

The site ID used in the Matomo server which will collect and analyze the gathered data.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## idsite Type

`integer` ([Matomo Site ID](cimp-properties-matomo-site-id.md))

## idsite Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## idsite Examples

```json
1
```

```json
2
```

```json
3
```

```json
345345
```
