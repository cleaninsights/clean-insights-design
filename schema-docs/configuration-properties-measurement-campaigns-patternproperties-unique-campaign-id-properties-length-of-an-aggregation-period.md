# Length of an Aggregation Period Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/aggregationPeriodLength
```

The length of the aggregation period in number of days. At the end of a period, the aggregated data will be sent to the analytics server.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## aggregationPeriodLength Type

`integer` ([Length of an Aggregation Period](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-length-of-an-aggregation-period.md))

## aggregationPeriodLength Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## aggregationPeriodLength Examples

```json
1
```

```json
2
```

```json
3
```

```json
7
```

```json
14
```

```json
30
```
