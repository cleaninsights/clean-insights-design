# Event Measurement Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items
```

A single aggregated measurement of a specific event.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## items Type

`object` ([Event Measurement](cimp-properties-event-measurement-event-measurement.md))

# items Properties

| Property                      | Type      | Required | Nullable       | Defined by                                                                                                                                                                                                                           |
| :---------------------------- | :-------- | :------- | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [category](#category)         | `string`  | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-category-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/category")      |
| [action](#action)             | `string`  | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-action-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/action")          |
| [name](#name)                 | `string`  | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-name.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/name")                         |
| [value](#value)               | `number`  | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-value.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/value")                       |
| [period_start](#period_start) | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-start-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/period_start") |
| [period_end](#period_end)     | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-end-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/period_end")     |
| [times](#times)               | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-number-of-times-occurred.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/times")          |

## category

A category identifier for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

`category`

*   is required

*   Type: `string` ([Event Category Identifier](cimp-properties-event-measurement-event-measurement-properties-event-category-identifier.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-category-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/category")

### category Type

`string` ([Event Category Identifier](cimp-properties-event-measurement-event-measurement-properties-event-category-identifier.md))

### category Constraints

**minimum length**: the minimum number of characters for this string is: `1`

### category Examples

```json
"Videos"
```

```json
"Music"
```

```json
"Games"
```

## action

An action identifier for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

`action`

*   is required

*   Type: `string` ([Event Action Identifier](cimp-properties-event-measurement-event-measurement-properties-event-action-identifier.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-action-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/action")

### action Type

`string` ([Event Action Identifier](cimp-properties-event-measurement-event-measurement-properties-event-action-identifier.md))

### action Constraints

**minimum length**: the minimum number of characters for this string is: `1`

### action Examples

```json
"Play"
```

```json
"Pause"
```

```json
"Duration"
```

```json
"Add Playlist"
```

```json
"Downloaded"
```

```json
"Clicked"
```

## name

An action name for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

`name`

*   is optional

*   Type: `string` ([Event Name](cimp-properties-event-measurement-event-measurement-properties-event-name.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-name.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/name")

### name Type

`string` ([Event Name](cimp-properties-event-measurement-event-measurement-properties-event-name.md))

### name Examples

```json
"Office Space"
```

```json
"Jonathan Coulton - Code Monkey"
```

```json
"kraftwerk-autobahn.mp3"
```

## value

A value for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

`value`

*   is optional

*   Type: `number` ([Event Value](cimp-properties-event-measurement-event-measurement-properties-event-value.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-event-value.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/value")

### value Type

`number` ([Event Value](cimp-properties-event-measurement-event-measurement-properties-event-value.md))

### value Examples

```json
0
```

```json
1
```

```json
1.5
```

```json
100
```

```json
56.44332
```

## period_start

Beginning of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

`period_start`

*   is required

*   Type: `integer` ([Start UNIX Epoch Timestamp](cimp-properties-event-measurement-event-measurement-properties-start-unix-epoch-timestamp.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-start-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/period_start")

### period_start Type

`integer` ([Start UNIX Epoch Timestamp](cimp-properties-event-measurement-event-measurement-properties-start-unix-epoch-timestamp.md))

### period_start Examples

```json
1602499451
```

## period_end

End of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

`period_end`

*   is required

*   Type: `integer` ([End UNIX Epoch Timestamp](cimp-properties-event-measurement-event-measurement-properties-end-unix-epoch-timestamp.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-end-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/period_end")

### period_end Type

`integer` ([End UNIX Epoch Timestamp](cimp-properties-event-measurement-event-measurement-properties-end-unix-epoch-timestamp.md))

### period_end Examples

```json
1602499451
```

## times

The number of times the visit to this page/scene/activity happened during the specified period.

`times`

*   is required

*   Type: `integer` ([Number of Times Occurred](cimp-properties-event-measurement-event-measurement-properties-number-of-times-occurred.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement-event-measurement-properties-number-of-times-occurred.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/times")

### times Type

`integer` ([Number of Times Occurred](cimp-properties-event-measurement-event-measurement-properties-number-of-times-occurred.md))

### times Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### times Examples

```json
1
```

```json
2
```

```json
3
```

```json
26745
```
