# HTTP User-Agent Header Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/ua
```

A  HTTP User-Agent. The user agent is used to detect the operating system and browser used.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## ua Type

`string` ([HTTP User-Agent Header](cimp-properties-http-user-agent-header.md))

## ua Examples

```json
"Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"
```
