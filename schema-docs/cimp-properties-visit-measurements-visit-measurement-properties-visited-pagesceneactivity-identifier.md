# Visited Page/Scene/Activity Identifier Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/action_name
```

Main identifier to track page/scene/activity visits in Matomo.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## action_name Type

`string` ([Visited Page/Scene/Activity Identifier](cimp-properties-visit-measurements-visit-measurement-properties-visited-pagesceneactivity-identifier.md))

## action_name Constraints

**minimum length**: the minimum number of characters for this string is: `1`

## action_name Examples

```json
"For example, Help / Feedback will create the Action Feedback in the category Help."
```
