# Start UNIX Epoch Timestamp Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/period_start
```

Beginning of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## period_start Type

`integer` ([Start UNIX Epoch Timestamp](cimp-properties-event-measurement-event-measurement-properties-start-unix-epoch-timestamp.md))

## period_start Examples

```json
1602499451
```
