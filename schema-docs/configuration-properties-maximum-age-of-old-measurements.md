# Maximum Age of Old Measurements Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxAgeOfOldData
```

The number in days of how long the SDK will try to keep sending old measurements. If the measurements become older than that, they will be purged.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## maxAgeOfOldData Type

`integer` ([Maximum Age of Old Measurements](configuration-properties-maximum-age-of-old-measurements.md))

## maxAgeOfOldData Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## maxAgeOfOldData Default Value

The default value is:

```json
100
```

## maxAgeOfOldData Examples

```json
14
```

```json
30
```

```json
100
```

```json
183
```

```json
365
```
